import React, { Component } from 'react';
import logo from './logo.svg';
import './App.css';
import Main from './components/main/Main';
import 'antd/dist/antd.css';
import { Route, Switch } from 'react-router-dom';
import PostPage from './components/post/PostPage';
import PostEdit from './components/main/PostEdit';

class App extends Component {
  render() {
    return (
        <div className="App">
          <Switch>
            <Route path='/newPost' render={ () => (
              <PostEdit />
            )} />
            <Route path='/posts/:id' render={ ({ match }) => (
              <PostPage id={ match.params.id } />
            )} />
            <Route path='/' render={ () => (
              // <PostList categoria={ undefined } />
              <Main />
            )} />


        </Switch>
        </div>
    );
  }
}

export default App;
