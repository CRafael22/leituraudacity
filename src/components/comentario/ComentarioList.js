import React from 'react';
import {
  Icon,
  Button,
  Spin,
  Card,
  Avatar,
  Tag
} from 'antd';
import { connect } from 'react-redux';
import { comentarioActions } from '../../actions';

class ComentarioList extends React.Component {

  constructor(props){
    super(props);

    this.handleVote = this.handleVote.bind(this);
  }

  handleVote (id, voto) {
    const valorVoto = voto == 'like-o' ? "upVote" : "downVote";
    console.log(valorVoto)
    this.props.votarComentarioComId(id, valorVoto);
  }

  render () {

    const {
      comentarios
    } = this.props;

    console.log("££33 RENDER DOS Comentarios")
    console.log(comentarios)

    const {
      listaComentarios
    } = comentarios;
    console.log(listaComentarios)


    return (
      <div style={{marginTop: "30px"}}>
        <h6>Comentarios</h6>
        {
          listaComentarios.length > 0 ?
          listaComentarios.map( item => (
            <Card title={Header(item, this.handleVote)} style={{ width: 600, marginTop: "10px" }} key={item.id}>
              {item.body}
            </Card>
          ))
          :
          <Card>Nao temos comentario para este post ainda.</Card>
        }
      </div>
    )
  }
}

const Header = (item, handleVote) => (
  <div className="row">
    <div className="col-sm-3">
      <Avatar icon="user"/> {item.author}
    </div>
    <div className="col-sm-6">
      <VoteIcon type="like-o" idComment={item.id} vote={handleVote} />
      <p style={{display: 'inline-block', margin: "10px "}}>{item.voteScore}</p>
      <VoteIcon type="dislike-o" idComment={item.id} vote={handleVote} />
    </div>
  </div>
);

const VoteIcon = ({ type, idComment, vote }) => (
  <span>
    <Button icon={type} onClick={ () => vote(idComment, type) } />
  </span>
);

const mapDispatchToProps = (dispatch) => {
  return {
    votarComentarioComId: (id, voto) => dispatch(comentarioActions.votarComentarioComId(id, voto))
  };
}

export default connect(null, mapDispatchToProps)(ComentarioList);
