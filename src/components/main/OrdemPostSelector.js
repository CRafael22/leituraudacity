import React, { Component } from 'react';
import { connect } from 'react-redux';
import { getPostActions } from '../../actions';
import { Radio } from 'antd';
const RadioGroup = Radio.Group;


class OrdemPostSelector extends React.Component {
  state = {
    value: 'voteScore',
  }

  onChange = (e) => {
    console.log('radio checked', e.target.value);
    this.setState({
      value: e.target.value,
    });
    this.props.ordenarPosts(e.target.value);
  }

  render() {
    return (
      <div>
        <p style={{display:'inline-block', marginRight:'10px'}}>Ordenar por:</p>
        <RadioGroup onChange={this.onChange} value={this.state.value}>
          <Radio value={'voteScore'}>Score</Radio>
          <Radio value={'timestamp'}>Data publicado</Radio>
        </RadioGroup>
      </div>

    );
  }
}

const mapDispatchToProps = (dispatch) => {

  return {
    ordenarPosts: (ordem) => dispatch(getPostActions.ordenarPosts(ordem))
  };
}

export default connect(null,mapDispatchToProps)(OrdemPostSelector);
