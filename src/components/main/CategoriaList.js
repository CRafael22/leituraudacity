import React, { Component } from 'react';
import { List, Card, Radio } from 'antd';
import { connect } from 'react-redux';
import { categoriaActions } from '../../actions';
import { Link } from 'react-router-dom';
import { withRouter } from 'react-router';

class CategoriaList extends Component{

  constructor(props) {
    super(props);
    console.log("££££ NO CONSTRUTOR DO CategoriaList £££££")
    console.log(this)

    this.state = {
      data: [],
      buttonRoute: "geral"
    }

    this.onChange = this.onChange.bind(this);
  }

  componentDidMount() {
    this.props.pegarCategorias();
  }

  onChange(e) {
    console.log(e);
    console.log(this)
    console.log("&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&")

    const rota = e.target.value;
    this.setState({ buttonRoute: rota });
    if (rota === 'geral'){
      this.props.history.push('/');
    } else {
      this.props.history.push(`/${e.target.value}`);
    }

  }

  render(){

    const {
      categorias
    } = this.props;

    console.log("RENDER")
    console.log(categorias)
    console.log(this.state)

     return (
         <div>
           <p style={{display: "inline-block", marginRight: 30}}>Categorias</p>
           <Radio.Group value={this.state.buttonRoute} onChange={this.onChange} style={{ marginBottom: 16 }}>
             <Radio.Button value="geral">Geral</Radio.Button>
             { categorias !== undefined ?
               categorias.map( item => (
                 // <Link to={{ pathname: `/${item.name}` }} key={item.name}>
                   <Radio.Button value={item.name}>{item.name}</Radio.Button>
                 // </Link>
               ))
               :
               null
             }
          </Radio.Group>
         </div>

     )
  }
};

const mapStateToProps = (state) => {

  console.log("!!!!!!!!!!")
  console.log(state)
  return {
    categorias: state.categorias.listaCategorias
  };
};

const mapDispatchToProps = (dispatch) => {

  return {
    pegarCategorias: () => dispatch(categoriaActions.pegarCategorias())
  };
}

export default withRouter(connect(mapStateToProps, mapDispatchToProps)(CategoriaList));
