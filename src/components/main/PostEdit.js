import React, { Component } from 'react';
import { connect } from 'react-redux';
import { modifyPostActions, categoriaActions } from '../../actions';
import { Radio, Input, DatePicker, Button, Select } from 'antd';
import pt_BR from 'antd/lib/locale-provider/pt_BR';
import moment from 'moment';
import { withRouter } from 'react-router';


class PostEdit extends React.Component {

  constructor(props){
    super(props);

    this.state = {
      title: '',
      body: '',
      author: '',
      timestamp: null,
      category: ''
    }

    props.pegarCategorias();
  }

  componentDidMount() {
    console.log("$$$$$ componentDidMount")
    this.props.pegarCategorias();
  }

  onChange = (e, stateField) => {
    this.setState({
      [stateField]: e.target.value,
    });
    console.log(this.state)
  }

  onSelectCategory = (category) => {
    this.setState({ category });
  }

  onChangeDatePicker = (date, dateString) => {
    this.setState({
      timestamp: moment(date).toDate()
    });
    console.log(this.state)
  }

  publishPost = () => {
    this.props.adicionarPost(this.state);
    this.props.history.push('/');
  }

  render() {

    const { categorias } = this.props;

    // No caso de categorias for undefined ao demorar a pagar as mesmas no estado
    const categoriasOptions = categorias !== undefined ? categorias : [];

    return (
      <div className="App Post-edit" style={{ }}>
        <div>
          <h3>Adicionar um novo post</h3>
        </div>
        <div className="Post-input">
          <Input placeholder="Titulo" onChange={ (e) => this.onChange(e, 'title') } />
        </div>
        <div className="Post-input">
          <Input.TextArea rows={8} onChange={ (e) => this.onChange(e, 'body') } />
        </div>

        <div className="Post-input">
          <Input placeholder="Autor" onChange={ (e) => this.onChange(e, 'author') } />
        </div>

        <Select defaultValue="Escolha a categoria" style={{ width: 240 }} onChange={(e) => this.onSelectCategory(e)}>
          {
            categoriasOptions.map( (cat) => {
              console.log(cat)
              const { name } = cat;
              return (
                <Select.Option value={name} key={name}>{name}</Select.Option>
              )
            })
          }
        </Select>

        <DatePicker onChange={this.onChangeDatePicker} />

        <Button type="primary" onClick={ () => this.publishPost() } >Adicionar post</Button>
      </div>

    );
  }
}

const mapDispatchToProps = (dispatch) => {

  return {
    adicionarPost: (post) => dispatch(modifyPostActions.adicionarPost(post)),
    pegarCategorias: () => dispatch(categoriaActions.pegarCategorias())
  };
}

const mapStateToProps = (state) => {

  console.log("!!!!!!!!!!")
  console.log(state)
  return {
    categorias: state.categorias.listaCategorias
  };
};

export default withRouter(connect(mapStateToProps, mapDispatchToProps)(PostEdit));
