import React, { Component } from 'react';
import { List, Card, Icon, Avatar } from 'antd';
import PostList from './PostList';
import CategoriaList from './CategoriaList';
import PostPage from './PostPage';
import PostEdit from './PostEdit';
import { Route, Switch } from 'react-router-dom';
import { withRouter } from 'react-router';

class Main extends Component{

  render(){
     return (
       <div>
         <CategoriaList />
         <Switch>
         <Route exact path='/' render={ () => (
           <PostList categoria={ undefined } />
         )} />
         <Route path='/:categoria' render={ ({ match }) => (
           <PostList categoria={ match.params.categoria } />
         )} />
       </Switch>
       </div>

     )
  }
};

export default withRouter(Main);
