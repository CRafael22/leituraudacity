import React, { Component } from 'react';
import { List, Card, Icon, Avatar, Button, Badge } from 'antd';
import { connect } from 'react-redux';
import { getPostActions, modifyPostActions } from '../../actions';
import OrdemPostSelector from './OrdemPostSelector';
import CategoriaList from './CategoriaList';
import { withRouter } from 'react-router';
import { Link } from 'react-router-dom';


class PostList extends Component{

  constructor(props) {
    super(props);
    this.handleVote = this.handleVote.bind(this);
    this.goToAddPost = this.goToAddPost.bind(this);
  }

  componentDidMount() {
    console.log("££££££££££ componentDidMount")
    this.props.pegarPosts();
  }

  goToAddPost() {
    this.props.history.push('/newPost');
  }

  handleVote(id, voto) {
    const valorVoto = voto == 'like-o' ? "upVote" : "downVote";
    console.log(valorVoto)
    this.props.votarPost(id, valorVoto);
  }

  render(){

    const {
      posts
    } = this.props;

    console.log("££££££££££ RENDER ££££££££")
    console.log(posts)

     return (
       <div>
        <OrdemPostSelector />
        <Button type="primary" onClick={ () => this.goToAddPost() } >Adicionar novo post</Button>
        <List
          itemLayout="vertical"
          size="large"
          pagination={{
            onChange: (page) => {
              console.log(page);
            },
            pageSize: 5,
          }}
          dataSource={posts}
          footer={<div><b>Leituras</b> footer part</div>}
          renderItem={item => (
            <List.Item
              key={item.id}
              actions={[
                <VoteIcon type="like-o" idPost={item.id} vote={this.handleVote} />,
                <div>{item.voteScore}</div>,
                <VoteIcon type="dislike-o" idPost={item.id} vote={this.handleVote} />,
                <IconText type="message" text="2" />]} >
              <List.Item.Meta
                avatar={<Avatar src={item.avatar} />}
                title={<Link to={{ pathname: `/posts/${item.id}` }}>{item.title}</Link>}
                description={item.body}
              />
              Author: {item.author}
            </List.Item>
          )}
        />
      </div>
     )
  }
};

const IconText = ({ type, text }) => (
  <span>
    <Icon
      type={type}
      style={{ marginRight: 8 }}
       />
    {text}
  </span>
);

const VoteIcon = ({ type, idPost, vote }) => (
  <span>
    <Button icon={type} onClick={ () => vote(idPost, type) } />
  </span>
);


const mapStateToProps = (state, ownProps) => {

  console.log("!!!! mapStateToProps POSTLISTS !!!!!!")
  console.log(state)
  console.log(ownProps)

  const { listaPosts } = state.posts;
  const { categoria } = ownProps;

  if (categoria !== undefined) {
    const filteredPosts = listaPosts.filter( post => post.category == ownProps.categoria );
  }

  const filteredPosts = categoria !== undefined ?
        listaPosts.filter( post => post.category == ownProps.categoria )
        :
        listaPosts;

  return {
    posts: filteredPosts
  };
};

const mapDispatchToProps = (dispatch) => {

  return {
    pegarPosts: () => dispatch(getPostActions.pegarPosts()),
    votarPost: (id, voto) => dispatch(modifyPostActions.votarPostComId(id, voto))
  };
}

export default withRouter(connect(mapStateToProps, mapDispatchToProps)(PostList));
