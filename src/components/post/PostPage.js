import React, { Component } from 'react';
import { connect } from 'react-redux';
import {
  Icon,
  Button,
  Spin,
  Card,
  Avatar,
  Tag
} from 'antd';
import moment from 'moment';
import { withRouter } from 'react-router';

import { comentarioActions, getPostActions } from '../../actions';
import ComentarioList from '../comentario/ComentarioList';


class PostPage extends Component {

  constructor(props){
    super(props);

    this.state = {
      title: '',
      body: '',
      author: '',
      timestamp: null,
      category: ''
    }
      console.log("NO CONSTRUOR DE POSTPAGE")

    //props.pegarCategorias();
  }

  componentDidMount() {
    console.log("$$$$$ componentDidMount")
    const {
      id,
      post,
      pegarComentarios,
      pegarPostComId
    } = this.props;

    pegarComentarios(id);

    // Se o this.props.post chegar como null eh pq ele nao estava no estado entao vamos pegar do servidor
    if (post === null) {
      pegarPostComId(id);
    }

  }

  publishPost = () => {
    this.props.adicionarPost(this.state);
    this.props.history.push('/');
  }

  render() {

    const {
      post,
      categorias,
      comentarios
    } = this.props;

    console.log("3££££££££££££333333333333333")
    console.log(comentarios)

    // No caso de categorias for undefined ao demorar a pagar as mesmas no estado
    const categoriasOptions = categorias !== undefined ? categorias : [];

    return (
      <div className="App Post-edit" style={{ }}>
      {
        post === null ?
        <Spin />
        :
        (
          <div>
            <div>
              <h3>{post.title}</h3>
            </div>

            <Card title={Header(post)} style={{ width: 600 }}>
              {post.body}
            </Card>

            <Button type="primary" onClick={ () => this.publishPost() }>Editar post</Button>

            <ComentarioList comentarios={comentarios} />
          </div>
        )
      }
      </div>
    );
  }
}

const Header = (post) => (
  <div className="row">
    <div className="col-sm-3">
      <Avatar icon="user"/> {post.author}
    </div>
    <div className="col-sm-3">
      <Tag>{post.category}</Tag>
    </div>
    <div className="col-sm-3">
      <Icon type="calendar" /> {moment(post.timestamp).format('ll')}
    </div>
    <div className="col-sm-3">
      <Icon type="like-o" /> {post.voteScore}
    </div>
  </div>
);

const mapDispatchToProps = (dispatch) => {

  return {
    pegarComentarios: (id) => dispatch(comentarioActions.pegarComentariosPost(id)),
    pegarPostComId: (id) => dispatch(getPostActions.pegarPostComId(id))
  };
}

const mapStateToProps = (state, ownProps) => {

  const { listaPosts } = state.posts;
  const { id } = ownProps;

  console.log("!!!!!!!!!!! PostPage !!!!!!!!!!!!")
  console.log(listaPosts)

  // Se o post com esta id estiver no estado entao eu pego direto, se nao tiver eh pq tinha dado refresh na pagina antes
  // Assim vou fazer requisicao para o servidor para pegar o post
  let post = null;
  if (listaPosts.length > 1){
    console.log("!!!!!!!!!!! TEM !!!!!!!!!!!!")
    post = listaPosts.find( item => item.id === id);
    console.log(post)
  } else if (listaPosts.length == 1) {
    post = listaPosts[0];
  }

  return {
    comentarios: state.comentarios,
    post
  };
};

export default withRouter(connect(mapStateToProps, mapDispatchToProps)(PostPage));
