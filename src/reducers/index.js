import { combineReducers } from 'redux';

import categoriaReducer from './categoriaReducer';
import postReducer from './postReducer';
import comentarioReducer from './comentarioReducer';

export default combineReducers({
  categorias: categoriaReducer,
  posts: postReducer,
  comentarios: comentarioReducer
});
