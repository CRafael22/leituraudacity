import { comentarioActionsTypes } from '../actions';

const {
  SALVAR_COMENTARIOS_POST,
  VOTAR_COMENTARIO
} = comentarioActionsTypes;

const comentarioReducer = (state = {}, action) => {

  const {
    listaComentarios,
    idPost
  } = action;

  switch (action.type) {
    case SALVAR_COMENTARIOS_POST:
      console.log(SALVAR_COMENTARIOS_POST)
      console.log({
        ...state,
        idPost,
        listaComentarios
      })
      return {
        ...state,
        idPost,
        listaComentarios
      };
    case VOTAR_COMENTARIO:
      console.log(VOTAR_COMENTARIO)
      let comentariosModificados = [...state.listaComentarios];

      comentariosModificados = comentariosModificados.map( post => {
        if (post.id === action.id) {
          post.voteScore = action.voto === 'upVote' ? post.voteScore+1 : post.voteScore-1;
        }
        return post;
      });
      console.log(comentariosModificados)
      return {
        ...state,
        listaComentarios: comentariosModificados,
      };
    default:
      return state;
  }
};

export default comentarioReducer;
