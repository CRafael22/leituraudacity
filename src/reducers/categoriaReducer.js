import { categoriaActionsTypes } from '../actions';

const {
  SALVAR_CATEGORIAS
} = categoriaActionsTypes;

const categoriaReducer = (state = {}, action) => {

  switch (action.type) {
    case SALVAR_CATEGORIAS:
      console.log(SALVAR_CATEGORIAS)
      console.log({
        ...state,
        categorias: action.categorias
      })
      return {
        ...state,
        listaCategorias: action.categorias
      };
    default:
      return state;
  }
};

export default categoriaReducer;
