import { postActionsTypes } from '../actions';

const {
  SALVAR_POSTS,
  SALVAR_POST,
  ORDENAR_POSTS,
  VOTAR_POST,
  ADICIONAR_POST
} = postActionsTypes;

const initialState = {
  listaPosts: [],
  ordem: 'voteScore'
};

const postReducer = (state = initialState, action) => {

  const {
    posts,
    ordem,
    post
  } = action;

  switch (action.type) {
    case SALVAR_POSTS:
      // Aqui eu vou receber os posts e ordena-los por voteScore que eh o padrao
      posts.sort( (a,b) => { return b.voteScore - a.voteScore });
      return {
        ...state,
        listaPosts: posts
      };
    case SALVAR_POST:
      return {
        ...state,
        listaPosts: [post]
      };
    case ADICIONAR_POST:
      console.log(ADICIONAR_POST)
      let statePosts = [...state.listaPosts];
      console.log(statePosts)
      statePosts.push(post);
      console.log(statePosts)
      // Aqui eu vou receber os posts e ordena-los por
      statePosts.sort( (a,b) => { return b.voteScore - a.voteScore });
      return {
        ...state,
        listaPosts: statePosts
      };
    case ORDENAR_POSTS:

      console.log(ORDENAR_POSTS)
      // Aqui eu vou receber os posts e ordena-los
      let postsOrdenados = [...state.listaPosts];
      postsOrdenados.sort( (a,b) => { return b[ordem] - a[ordem] });
      console.log(postsOrdenados)
      return {
        ...state,
        listaPosts: postsOrdenados,
        ordem: ordem
      };
    case VOTAR_POST:
      console.log(VOTAR_POST)
      let postsModificados = [...state.listaPosts];

      postsModificados = postsModificados.map( post => {
        if (post.id === action.id) {
          post.voteScore = action.voto === 'upVote' ? post.voteScore+1 : post.voteScore-1;
        }
        return post;
      });
      console.log(postsModificados)
      return {
        ...state,
        listaPosts: postsModificados,
      };
    default:
      return state;
  }
};

export default postReducer;
