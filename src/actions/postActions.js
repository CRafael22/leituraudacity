
const salvarPosts = (posts) => {

  return {
    type: SALVAR_POSTS,
    posts
  };
}

const salvarPostOnState = (post) => {

  return {
    type: SALVAR_POST,
    post
  };
}

const adicionarPostOnState = (post) => {

  return {
    type: ADICIONAR_POST,
    post
  };
}

const ordenarPosts = (ordem) => {

  return {
    type: ORDENAR_POSTS,
    ordem
  };
}

const votarNoPost = (id, voto) => {
  return {
    type: VOTAR_POST,
    id,
    voto
  };
}

const pegarPosts = () => {

  return (dispatch) => {

    fetch('http://localhost:3001/posts', { headers: { 'Authorization': 'whatever-you-want' }})
    .then( res => { return res.json() })
    .then( data => {
       console.log(data);
       dispatch(salvarPosts(data));
    }).catch( (err) => {
      console.log(err);
    });
  }
}

const pegarPostComId = (id) => {

  return (dispatch) => {

    fetch(`http://localhost:3001/posts/${id}`, { headers: { 'Authorization': 'whatever-you-want' }})
    .then( res => { return res.json() })
    .then( data => {
       console.log(data);
       dispatch(salvarPostOnState(data));
    }).catch( (err) => {
      console.log(err);
    });
  }
}

const votarPostComId = (id, voto) => {

  return (dispatch) => {

    console.log("NO action")
    console.log(id, voto)

    fetch('http://localhost:3001/posts/'+id,
    {
      method: 'POST',
      headers : new Headers({
             'Content-Type': 'application/json',
             'Authorization': 'whatever-you-want'
      }),
      // headers : { 'Authorization': 'whatever-you-want', 'Content-Type':'application/json' },
      body:JSON.stringify({
        option: voto
      })
    }
    ).then( res => { return res.json() })
    .then( data => {
       console.log(data);
       dispatch(votarNoPost(id, voto));
    }).catch( (err) => {
      console.log(err);
    });
  }
}

const adicionarPost = (post) => {

  return (dispatch) => {

    console.log("NO action")
    console.log(post)
    console.log(+post.timestamp)

    const {
      id,
      title,
      body,
      author,
      category,
      timestamp
    } = post;

    fetch('http://localhost:3001/posts/',
    {
      method: 'POST',
      headers : new Headers({
             'Content-Type': 'application/json',
             'Authorization': 'whatever-you-want'
      }),
      // headers : { 'Authorization': 'whatever-you-want', 'Content-Type':'application/json' },
      body:JSON.stringify({
        id,
        title,
        body,
        author,
        category,
        timestamp: +timestamp
      })
    }
    ).then( res => { return res.json() })
    .then( data => {
       console.log("!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!1!!!!!!1")
       console.log(data);
       dispatch(adicionarPostOnState(post));
    }).catch( (err) => {
      console.log(err);
    });
  }
}


const getPostActions = {
  pegarPosts,
  ordenarPosts,
  pegarPostComId,
};

const modifyPostActions = {
  votarPostComId,
  adicionarPost,
};

export { getPostActions, modifyPostActions };

export const SALVAR_POSTS = 'SALVAR_POSTS';
export const SALVAR_POST = 'SALVAR_POST';
export const ORDENAR_POSTS = 'ORDENAR_POSTS';
export const VOTAR_POST = 'VOTAR_POST';
export const ADICIONAR_POST = 'ADICIONAR_POST';
