import * as categoria from './categoriaActions';
import * as post from './postActions';
import * as comentario from './comentarioActions';

const {
  categoriaActions,
  ...categoriaActionsTypes
} = categoria;

const {
  getPostActions,
  modifyPostActions,
  ...postActionsTypes
} = post;

const {
  comentarioActions,
  ...comentarioActionsTypes
} = comentario;

export {
  categoriaActions,
  getPostActions,
  modifyPostActions,
  comentarioActions
};

export {
  categoriaActionsTypes,
  postActionsTypes,
  comentarioActionsTypes
};
