
const salvarCategorias = (categorias) => {
  return {
    type: SALVAR_CATEGORIAS,
    categorias
  };
}

const pegarCategorias = () => {
  return (dispatch) => {

    fetch('http://localhost:3001/categories', { headers: { 'Authorization': 'whatever-you-want' }})
    .then( res => { return res.json() })
    .then( data => {
       console.log(data);
       dispatch(salvarCategorias(data.categories));
    }).catch( function(err) {
      console.log(err);
    });
  }
}


const categoriaActions = {
  pegarCategorias
};

export { categoriaActions };

export const SALVAR_CATEGORIAS = 'SALVAR_CATEGORIAS';
