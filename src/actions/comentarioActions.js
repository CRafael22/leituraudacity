
const salvarComentarios = (idPost, listaComentarios) => {
  return {
    type: SALVAR_COMENTARIOS_POST,
    idPost,
    listaComentarios
  };
}

const votarNoComentario = (id, voto) => {
  return {
    type: VOTAR_COMENTARIO,
    id,
    voto
  };
}

const pegarComentariosPost = (id) => {
  return (dispatch) => {

    fetch(`http://localhost:3001/posts/${id}/comments`, { headers: { 'Authorization': 'whatever-you-want' }})
    .then( res => { return res.json() })
    .then( data => {
       console.log("$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$")
       console.log(data);
       dispatch(salvarComentarios(id, data));
    }).catch( function(err) {
      console.log(err);
    });
  }
}

const votarComentarioComId = (id, voto) => {

  return (dispatch) => {

    console.log("NO action")
    console.log(id, voto)

    fetch('http://localhost:3001/comments/'+id,
    {
      method: 'POST',
      headers : new Headers({
             'Content-Type': 'application/json',
             'Authorization': 'whatever-you-want'
      }),
      // headers : { 'Authorization': 'whatever-you-want', 'Content-Type':'application/json' },
      body:JSON.stringify({
        option: voto
      })
    }
    ).then( res => { return res.json() })
    .then( data => {
       console.log(data);
       dispatch(votarNoComentario(id, voto));
    }).catch( (err) => {
      console.log(err);
    });
  }
}


const comentarioActions = {
  pegarComentariosPost,
  votarComentarioComId
};

export { comentarioActions };

export const SALVAR_COMENTARIOS_POST = 'SALVAR_COMENTARIOS_POST';
export const VOTAR_COMENTARIO = 'VOTAR_COMENTARIO';
